//
//  BaseNetworking.swift
//  BaseNetworking
//
//  Created by Hesham Ali on 2/18/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire
import RxSwift
import SwiftKeychainWrapper
public class NetworkHandler {

    private let disposeBag = DisposeBag()
    private var appError: AppError?
    private let environment: Environment = .development
    public func performRequest<T: Codable>(url: String, method: HTTPMethod, params: Parameters?, encoding: ParameterEncoding) -> Observable<(T?, AppError?)> {
        guard let url = URL(string: environment.rawValue + url) else {
            return Observable.just((nil, AppError(message: .invalidUrl, errorCode: nil)))
        }
        let headers: HTTPHeaders = ["Authorization": "Basic \(getBasicAuthenticationValue())", "Content-Type": "application/json"]
        return Observable<(T?, AppError?)>.create { (observer) -> Disposable in
            return RxAlamofire.requestData(method, url, parameters: params, encoding: encoding, headers: headers).subscribe(onNext: { (_, data) in
                do {
                    let decoder = JSONDecoder()
                    let successType = try decoder.decode(T.self, from: data)
                    observer.onNext((successType, nil))
                } catch let jsonError {
                    observer.onError(jsonError)
                }
            }, onError: { (error) in
                guard let error = error as? AFError else {
                    return
                }
                let appError = AppError(message: error.appErrorMessage, errorCode: error.responseCode)
                observer.onNext((nil, appError))
            })
        }
    }

    private func getBasicAuthenticationValue() -> String {
        guard let username = KeychainWrapper.standard.string(forKey: Constants.KeyChainKeys.username),
              let password = KeychainWrapper.standard.string(forKey: Constants.KeyChainKeys.password),
              let authenticationData = "\(username):\(password)".data(using: .utf8)
              else {
            return ""
        }
        return authenticationData.base64EncodedString(options: [])
    }
}
