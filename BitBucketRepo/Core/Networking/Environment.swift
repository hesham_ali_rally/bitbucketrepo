//
//  Environment.swift
//  Neurocrine
//
//  Created by Hesham Ali on 2/2/21.
//

import Foundation
public enum Environment: String {
    case development = "https://api.bitbucket.org/2.0/"
    case staging = "testing.com"
    case production = "production.com"
    case mock = "https://run.mocky.io/v3/"
}
