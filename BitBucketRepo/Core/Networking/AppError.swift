//
//  AppError.swift
//  BaseNetworking
//
//  Created by Hesham Ali on 2/18/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import Alamofire
public struct AppError: Error, Codable {
    let message: String?
    let errorCode: Int?
    init(message appMessage: ApiErrorMessages, errorCode: Int?) {
        self.message = appMessage.rawValue
        self.errorCode = errorCode
    }

    init(message: String, errorCode: Int? = nil) {
        self.message = message
        self.errorCode = errorCode
    }
}

enum ApiErrorMessages: String {
    // MARK: - Network Messages
    case noInternetConnection = "It looks like your internet connection is off. Please turn it on and try again"
    case requestTimeout = "Request Timeout. Please check your network connection and try again"
    case defaultApiFaliureMessage = "An error occurred while retrieving data"
    case invalidUrl = "Invalid URL"
}

extension AFError {
    var appErrorMessage: String {
        var errorMessage = ApiErrorMessages.defaultApiFaliureMessage.rawValue

        if let underlyingError = self.underlyingError {
            if let urlError = underlyingError as? URLError {
                switch urlError.code {
                case .timedOut:
                    errorMessage = ApiErrorMessages.requestTimeout.rawValue

                case .notConnectedToInternet:
                    errorMessage = ApiErrorMessages.noInternetConnection.rawValue

                default:
                    errorMessage = ApiErrorMessages.defaultApiFaliureMessage.rawValue
                    #if DEBUG
                        errorMessage = self.localizedDescription
                    #endif
                }
            }
        }
        return errorMessage
    }
}
