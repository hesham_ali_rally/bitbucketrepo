//
//  Date.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
extension String {
    func toDateString() -> String {
        return self.components(separatedBy: "T")[0]
    }
}
