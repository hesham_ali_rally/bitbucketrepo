//
//  AppCoordinator.swift
//  Neurocrine
//
//  Created by Hesham Ali on 2/10/21.
//

import Foundation
import RxSwift
import UIKit
class AppCoordinator: Coordinator, CoordinatorProtocol {
    let viewFactory: ViewFactory!
    let window: UIWindow?
    var disposeBag: DisposeBag = DisposeBag()
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    init(window: UIWindow? = nil, viewFactory: ViewFactory) {

        self.viewFactory = viewFactory
        self.window = window
        super.init()
    }

    var navigationController: UINavigationController! {
        window?.rootViewController as? UINavigationController
    }

    func start() {

        let loginViewController = setupLoginViewController()
        attachRootViewController(viewController: loginViewController)
    }

    private func setupLoginViewController() -> LoginViewController {
        let loginViewController = viewFactory.loginViewController
        loginViewController.loginViewModel.outputs.navigateToMainScreen.subscribe { [weak self] (_) in
            guard let self = self else {
                return
            }
            self.attachRootViewController(viewController: self.setupTabBarViewController())
        }.disposed(by: disposeBag)
        return loginViewController
    }

    private func setupTabBarViewController() -> UITabBarController {
        let tabBarController = self.viewFactory.tabBarViewController
        setupRepositoryListListeners()
        setupProfileSettingsListeners()
        return tabBarController
    }

    private func setupRepositoryListListeners() {
        guard let repositoryListVC = self.viewFactory.tabBarViewController.viewControllers?.first as? RepositoryListViewController else {
            return
        }
        repositoryListVC.repositoryListViewModel.outputs.selectedRepository.subscribe(onNext: { [weak self] (repository) in
            guard let self = self else {
                return
            }
            let repositoryDetailsViewController = self.setupRepositoryDetails()
            repositoryDetailsViewController.repositoryDetailsViewModel.inputs.repositorySelected.onNext(repository)
            self.pushViewController(viewController: repositoryDetailsViewController)
        }).disposed(by: disposeBag)
    }

    private func setupProfileSettingsListeners() {
        guard let profileSettingsViewController = self.viewFactory.tabBarViewController.viewControllers?[1] as? ProfileSettingsViewController else {
            return
        }
        profileSettingsViewController.profileSettingsViewModel.outputs.navigateToLogin.subscribe { [weak self] (_) in
            self?.disposeBag = DisposeBag()
            self?.start()
        }.disposed(by: disposeBag)

    }

    private func setupRepositoryDetails() -> RepositoryDetailsViewController {
        let repositoryDetails = viewFactory.repositoryDetailsViewController
        return repositoryDetails
    }

    private func attachRootViewController(viewController: UIViewController) {
        let nav = UINavigationController(rootViewController: viewController)
        window?.rootViewController = nav
    }

    private func pushViewController(viewController: UIViewController) {
        self.navigationController.pushViewController(viewController, animated: true)
    }
}
