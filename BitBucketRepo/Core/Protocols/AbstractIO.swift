//
//  AbstractIO.swift
//  Wikipedia Location
//
//  Created by Hesham Ali on 8/19/21.
//

import Foundation
import RxSwift
final class EmptyInputs {}
final class EmptyOutputs {}

open class AbstractIO<Inputs, Outputs> {
    var inputs: Inputs
    var outputs: Outputs
    var subscriptions: DisposeBag = DisposeBag()

    // subclasses must call this method in their initializer
    // swiftlint:disable force_cast
    internal init(inputs: Inputs? = nil, outputs: Outputs? = nil) {
        self.inputs = inputs ?? (EmptyInputs() as! Inputs)
        self.outputs = outputs ?? (EmptyOutputs() as! Outputs)
    }

}
