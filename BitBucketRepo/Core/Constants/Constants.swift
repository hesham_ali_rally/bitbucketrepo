//
//  Constants.swift
//  Neurocrine
//
//  Created by Hesham Ali on 2/3/21.
//

import Foundation
import Alamofire
public class Constants {
    public class KeyChainKeys {
        public static let username = "username"
        public static let password = "password"
    }
    public class APIs {
        public static let user = "user"
        public static let repositories = "repositories"
    }
}
