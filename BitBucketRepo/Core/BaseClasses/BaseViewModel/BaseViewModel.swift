//
//  BaseViewModel.swift
//  BaseViewModel
//
//  Created by Hisham on 2/23/20.
//  Copyright © 2020 Hisham. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
open class BaseViewModel<Input, Output>: AbstractIO<Input, Output> {

    private(set) var dataStateSubject: BehaviorSubject<DataState> = BehaviorSubject(value: .none)
    private var apiExceptionSubject: PublishSubject<AppError> = PublishSubject<AppError>()
    let disposeBag = DisposeBag()

    public var environment: Environment {
        .development
    }

    var apiExceptionObservable: Observable<AppError> {
        apiExceptionSubject.asObservable()
    }

    func manageViewState<T>(publisher: Observable<(T?, AppError?)>, state: DataState = .loading) -> Observable<(T?, AppError?)> {
        return publisher.do(onNext: { [weak self] (successModel, appError) in
            self?.handleSubscriberCompletion(successModel: successModel, appError: appError)
        }, onSubscribe: {
            self.dataStateSubject.onNext(state)
        })
    }

    func sendToSubjects<T: Codable>(successSubject: PublishSubject<T>, response: (T?, AppError?)) {
        if let success = response.0 {
            successSubject.onNext(success)
        }
        if let appError = response.1 {
            apiExceptionSubject.onNext(appError)
        }
    }

    private func handleSubscriberCompletion<T>(successModel: T?, appError: AppError?) {
        if let appError = appError {
            self.dataStateSubject.onNext(.error(err: appError))
        } else {
            self.dataStateSubject.onNext(.finished)
        }
    }

    func sendError(error: AppError) {
        self.apiExceptionSubject.onNext(error)
    }

    func clearDataState() {
        dataStateSubject.onNext(.none)
    }

    func listenWithDisposing(disposable: Disposable) {
        disposable.disposed(by: disposeBag)
    }

}
