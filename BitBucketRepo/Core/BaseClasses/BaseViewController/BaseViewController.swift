//
//  BaseViewController.swift
//  RowaadApplication
//
//  Created by Hisham on 2/23/20.
//  Copyright © 2020 Hisham. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD
open class BaseViewController: UIViewController {
    var disposeBag = DisposeBag()
    open override func viewDidLoad() {
        super.viewDidLoad()
    }

    func subscribeForDataState<Input, Output>(viewModel: BaseViewModel<Input, Output>) {
        viewModel.dataStateSubject.subscribe(onNext: { [weak self] (dataState) in
            self?.handleDataState(dataState: dataState, viewModel: viewModel)
        }).disposed(by: disposeBag)

    }

    func subscribeForAppError<Input, Output>(viewModel: BaseViewModel<Input, Output>) {
        viewModel.apiExceptionObservable.subscribe(onNext: { [weak self] (appError) in
            guard let self = self else {
                return
            }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.showAlert(title: "Error", message: appError.message ?? "")
        }).disposed(by: disposeBag)
    }

    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

    private func handleDataState<Input, Output>(dataState: DataState, viewModel: BaseViewModel<Input, Output>) {
        switch dataState {
        case .loading:
            MBProgressHUD.showAdded(to: self.view, animated: true)
        case .finished, .none:
            MBProgressHUD.hide(for: self.view, animated: true)
        case .success(message: let msg):
            MBProgressHUD.hide(for: self.view, animated: true)
            self.showAlert(title: "Success", message: msg)
            viewModel.clearDataState()

        case .error(err: let err):
            let errorMessage = err.message ?? ApiErrorMessages.defaultApiFaliureMessage.rawValue
            self.showAlert(title: "Error", message: errorMessage)
            viewModel.clearDataState()

        }
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
