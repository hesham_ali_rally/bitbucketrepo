//
//  MainRepo.swift
//  Neurocrine
//
//  Created by Hesham Ali on 2/4/21.
//

import Foundation
import Alamofire

class BaseRepo {
    var networkHandler: NetworkHandler

    init(networkHandler: NetworkHandler = NetworkHandler()) {
        self.networkHandler = networkHandler
    }
}
