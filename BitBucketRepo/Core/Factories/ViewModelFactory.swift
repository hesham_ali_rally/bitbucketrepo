//
//  ViewModelFactory.swift
//  Neurocrine
//
//  Created by Hesham Ali on 2/17/21.
//

import Foundation
import Combine
protocol ViewModelFactoryProtocol {
    var repoFactory: RepoFactoryProtocol { get }
    var loginViewModel: LoginViewModel { get }
    var repositoryListViewModel: RepositoryListViewModel { get }
    var repositoryDetailsViewModel: RepositoryDetailsViewModel { get }
    var profileSettingsViewModel: ProfileSettingsViewModel { get }
}

class ViewModelFactory: ViewModelFactoryProtocol {

    var repoFactory: RepoFactoryProtocol = RepoFactory()
    let loginViewModel: LoginViewModel
    let repositoryListViewModel: RepositoryListViewModel
    var profileSettingsViewModel: ProfileSettingsViewModel
    var repositoryDetailsViewModel: RepositoryDetailsViewModel {
        RepositoryDetailsViewModel()
    }
    init() {
        loginViewModel = LoginViewModel(userRepo: repoFactory.userRepo)
        repositoryListViewModel = RepositoryListViewModel(repositoryFetcher: repoFactory.repositoryFetcher)
        profileSettingsViewModel = ProfileSettingsViewModel(userRepo: repoFactory.userRepo)
    }
}
