//
//  ViewFactory.swift
//  Neurocrine
//
//  Created by Hesham Ali on 2/16/21.
//

import Foundation
import RxSwift
import UIKit
class ViewFactory {
    let storyboard: UIStoryboard!
    let viewModelFactory: ViewModelFactoryProtocol!
    init(viewModelFactory: ViewModelFactoryProtocol) {
        self.viewModelFactory = viewModelFactory
        self.storyboard = UIStoryboard(name: "Main", bundle: nil)
    }

    var loginViewController: LoginViewController {
        let loginVC = storyboard.instantiateInitialViewController { [weak self] (coder) -> LoginViewController? in
            guard let self = self else {
                fatalError("Could not instantiate LoginViewController")
            }
            return LoginViewController(coder: coder, loginViewModel: self.viewModelFactory.loginViewModel)
        }
        guard let loginView = loginVC else {
            fatalError("Could not instantiate LoginViewController")
        }
        return loginView
    }

    var repositoryListViewController: RepositoryListViewController {
        let repositoryListVC = storyboard.instantiateViewController(identifier: "RepositoryListViewController") { [weak self] (coder) -> RepositoryListViewController? in
            guard let self = self else {
                fatalError("Could not instantiate LoginViewController")
            }
            return RepositoryListViewController(coder: coder, repositoryListViewModel: self.viewModelFactory.repositoryListViewModel)
        }
        return repositoryListVC
    }

    var tabBarViewController: UITabBarController {
        guard let mainTabBar = storyboard.instantiateViewController(withIdentifier: "BitBuckTabBarViewController") as? UITabBarController else {
            fatalError("Could not instantiate tabBarViewController")
        }
        mainTabBar.setViewControllers([repositoryListViewController, profileSettingViewController], animated: true)
        return mainTabBar
    }

    var repositoryDetailsViewController: RepositoryDetailsViewController {
        let repositoryDetailsVC = storyboard.instantiateViewController(identifier: "RepositoryDetailsViewController") { [weak self] (coder) -> RepositoryDetailsViewController? in
            guard let self = self else {
                fatalError("Could not instantiate RepositoryDetailsViewController")
            }
            return RepositoryDetailsViewController(coder: coder, repositoryDetailsViewModel: self.viewModelFactory.repositoryDetailsViewModel)
        }
        return repositoryDetailsVC
    }

    var profileSettingViewController: ProfileSettingsViewController {
        let profileSettingsVC = storyboard.instantiateViewController(identifier: "ProfileSettingsViewController") { [weak self] (coder) -> ProfileSettingsViewController? in
            guard let self = self else {
                fatalError("Could not instantiate ProfileSettingsViewController")
            }
            return ProfileSettingsViewController(coder: coder, profileSettingsViewModel: self.viewModelFactory.profileSettingsViewModel)
        }
        return profileSettingsVC
    }

}
