//
//  RepoFactory.swift
//  Neurocrine
//
//  Created by Hesham Ali on 2/23/21.
//

import Foundation

protocol RepoFactoryProtocol {
    var userRepo: UserRepo { get }
    var repositoryFetcher: RepositoryFetcher { get }
}

class RepoFactory: RepoFactoryProtocol {
    var userRepo: UserRepo {
        UserRepo()
    }

    var repositoryFetcher: RepositoryFetcher {
        RepositoryFetcher()
    }
}
