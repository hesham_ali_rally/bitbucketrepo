//
//  User.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
import Foundation

// MARK: - User
struct User: Codable {
    let username, displayName, accountID: String
    let links: Links
    let nickname, createdOn: String
    let isStaff: Bool
    let accountStatus, type, uuid: String

    enum CodingKeys: String, CodingKey {
        case username
        case displayName = "display_name"
        case accountID = "account_id"
        case links, nickname
        case createdOn = "created_on"
        case isStaff = "is_staff"
        case accountStatus = "account_status"
        case type, uuid
    }
}

// MARK: - Links
struct Links: Codable {
    let hooks, linksSelf, repositories, html: Avatar
    let avatar, snippets: Avatar

    enum CodingKeys: String, CodingKey {
        case hooks
        case linksSelf = "self"
        case repositories, html, avatar, snippets
    }
}

// MARK: - Avatar
struct Avatar: Codable {
    let href: String
}
