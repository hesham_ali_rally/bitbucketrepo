//
//  LoginVIewModel.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
import RxSwift
import RxCocoa
final class LoginInput {
    let userAuthenticationSubject = PublishSubject<(String, String)>()
}

final class LoginOutput {
    let userSuccessSubject = PublishSubject<User>()
    let isLoggedIn: PublishRelay<Bool> = PublishRelay()
    let navigateToMainScreen: PublishSubject<Void> = PublishSubject()
}

class LoginViewModel: BaseViewModel<LoginInput, LoginOutput> {
    private let userRepo: UserRepo
    init(userRepo: UserRepo) {
        self.userRepo = userRepo
        let input = LoginInput()
        let output = LoginOutput()
        super.init(inputs: input, outputs: output)
        listenWithDisposing(disposable: inputs.userAuthenticationSubject.subscribe(onNext: { [weak self] (username, password) in
            guard let self = self else {
                return
            }
            self.listenWithDisposing(disposable: self.manageViewState(publisher: userRepo.fetchUser(username: username, password: password)).subscribe { (user, _) in
                self.outputs.isLoggedIn.accept(user != nil ? true : false)
            })
        }))
    }
}
