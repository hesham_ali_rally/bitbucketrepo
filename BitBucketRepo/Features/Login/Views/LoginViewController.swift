//
//  LoginViewController.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    var loginViewModel: LoginViewModel!

    init?(coder: NSCoder, loginViewModel: LoginViewModel) {
        self.loginViewModel = loginViewModel
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeForAppError(viewModel: loginViewModel)
        subscribeForDataState(viewModel: loginViewModel)
        loginButton.rx.tap.subscribe { [weak self] (_) in
            self?.loginViewModel.inputs.userAuthenticationSubject.onNext((self?.usernameTextfield.text ?? "", self?.passwordTextField.text ?? ""))
        }.disposed(by: disposeBag)

        loginViewModel.outputs.isLoggedIn.subscribe(onNext: { [weak self] (isLoggedIn) in
            if isLoggedIn {
                self?.loginViewModel.outputs.navigateToMainScreen.onNext(())
            }
        }).disposed(by: disposeBag)

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
