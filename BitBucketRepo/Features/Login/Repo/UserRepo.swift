//
//  UserRepo.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
import RxSwift
import SwiftKeychainWrapper
import Alamofire
class UserRepo: BaseRepo {
    func fetchUser(username: String, password: String) -> Observable<(User?, AppError?)> {
        KeychainWrapper.standard.set(username, forKey: Constants.KeyChainKeys.username)
        KeychainWrapper.standard.set(password, forKey: Constants.KeyChainKeys.password)
        return networkHandler.performRequest(url: Constants.APIs.user, method: .get, params: nil, encoding: URLEncoding.default)
    }
}
