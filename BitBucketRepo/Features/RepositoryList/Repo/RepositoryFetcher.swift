//
//  RepositoryFetcher.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
import Alamofire
import RxSwift
class RepositoryFetcher: BaseRepo {
    func fetchRepos(role: Role) -> Observable<(Repository?, AppError?)> {
        return networkHandler.performRequest(url: Constants.APIs.repositories, method: .get, params: ["role": role.rawValue], encoding: URLEncoding.queryString)
    }
}
