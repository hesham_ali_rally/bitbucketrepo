//
//  Repository.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation

// MARK: - Repository
struct Repository: Codable {
    let pagelen: Int
    let values: [InternalRepository]
}

// MARK: - Value
struct InternalRepository: Codable {
    let scm: String
    let hasWiki: Bool
    let uuid: String
    let links: InternalRepositoryLinks
    let forkPolicy, fullName, name: String
    let project: Project
    let language, createdOn: String
    let mainbranch: Mainbranch
    let workspace: Project
    let hasIssues: Bool
    let owner: Owner
    let updatedOn: String
    let size: Int
    let type, slug: String
    let isPrivate: Bool
    let valueDescription: String

    enum CodingKeys: String, CodingKey {
        case scm
        case hasWiki = "has_wiki"
        case uuid, links
        case forkPolicy = "fork_policy"
        case fullName = "full_name"
        case name, project, language
        case createdOn = "created_on"
        case mainbranch, workspace
        case hasIssues = "has_issues"
        case owner
        case updatedOn = "updated_on"
        case size, type, slug
        case isPrivate = "is_private"
        case valueDescription = "description"
    }
}

// MARK: - ValueLinks
struct InternalRepositoryLinks: Codable {
    let watchers, branches, tags, commits: Avatar
    let clone: [Clone]
    let linksSelf, source, html, avatar: Avatar
    let hooks, forks, downloads, pullrequests: Avatar

    enum CodingKeys: String, CodingKey {
        case watchers, branches, tags, commits, clone
        case linksSelf = "self"
        case source, html, avatar, hooks, forks, downloads, pullrequests
    }
}

// MARK: - Clone
struct Clone: Codable {
    let href: String
    let name: String
}

// MARK: - Mainbranch
struct Mainbranch: Codable {
    let type, name: String
}

// MARK: - Owner
struct Owner: Codable {
    let displayName, uuid: String
    let links: OwnerLinks
    let type, nickname, accountID: String

    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case uuid, links, type, nickname
        case accountID = "account_id"
    }
}

// MARK: - OwnerLinks
struct OwnerLinks: Codable {
    let linksSelf, html, avatar: Avatar

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case html, avatar
    }
}

// MARK: - Project
struct Project: Codable {
    let links: OwnerLinks
    let type, name: String
    let key: String?
    let uuid: String
    let slug: String?
}

enum Role: String {
    case admin
    case owner
    case contributor
    case member
}
