//
//  RepositoryListViewModel.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
import RxSwift
import RxCocoa
final class RepositoryListInputs {
    let loadRepositoryList = PublishSubject<Void>()
}

final class RepositoryListOutputs {
    let repositories = BehaviorRelay<[InternalRepository]>(value: [])
    let isRepositoriesEmpty = BehaviorRelay<Bool>(value: false)
    let selectedRepository = PublishSubject<InternalRepository>()
}

class RepositoryListViewModel: BaseViewModel<RepositoryListInputs, RepositoryListOutputs> {
    private let repositoryFetcher: RepositoryFetcher
    init(repositoryFetcher: RepositoryFetcher) {
        self.repositoryFetcher = repositoryFetcher
        let repositoryInputs = RepositoryListInputs()
        let repositoryOutputs = RepositoryListOutputs()
        super.init(inputs: repositoryInputs, outputs: repositoryOutputs)

        listenWithDisposing(disposable: inputs.loadRepositoryList.subscribe(onNext: { [weak self] (_) in
            guard let self = self else {
                return
            }
            self.listenWithDisposing(disposable: self.manageViewState(publisher: self.repositoryFetcher.fetchRepos(role: Role.owner)).subscribe { (repositories, _) in
                self.outputs.repositories.accept(repositories?.values ?? [])
                self.outputs.isRepositoriesEmpty.accept(repositories?.values.isEmpty ?? true)
            })
        }))

    }
}
