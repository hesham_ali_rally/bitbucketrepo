//
//  RepositoryTableViewCell.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    static let identifier = "RepositoryTableViewCell"
    static let nib = UINib(nibName: identifier, bundle: .main)
    @IBOutlet weak var repoTitle: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    func configureCell(internalRepo: InternalRepository) {
        repoTitle.text = internalRepo.name
        createdAt.text = internalRepo.createdOn.toDateString()
    }

}
