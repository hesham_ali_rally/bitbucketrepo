//
//  RepositoryListViewController.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import UIKit
import RxCocoa
import RxSwift
class RepositoryListViewController: BaseViewController {
    var repositoryListViewModel: RepositoryListViewModel!

    @IBOutlet weak var noRepositoriesLabel: UILabel!
    @IBOutlet weak var repositoryTableView: UITableView!
    init?(coder: NSCoder, repositoryListViewModel: RepositoryListViewModel) {
        self.repositoryListViewModel = repositoryListViewModel
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeForAppError(viewModel: repositoryListViewModel)
        subscribeForDataState(viewModel: repositoryListViewModel)
        repositoryTableView.register(RepositoryTableViewCell.nib, forCellReuseIdentifier: RepositoryTableViewCell.identifier)
        bindRepositoriesToTable()
        subscribeToEmptyRepos()
        subscribeToRepositorySelection()
        repositoryListViewModel.inputs.loadRepositoryList.onNext(())
        // Do any additional setup after loading the view.
    }

    func bindRepositoriesToTable() {
        self.repositoryListViewModel.outputs.repositories.asObservable()
            .bind(to: self.repositoryTableView
                .rx
                    .items(cellIdentifier: RepositoryTableViewCell.identifier,
                       cellType: RepositoryTableViewCell.self)) { _, repository, cell in
                        cell.configureCell(internalRepo: repository)
        }
        .disposed(by: disposeBag)
    }

    func subscribeToEmptyRepos() {
        repositoryListViewModel.outputs.isRepositoriesEmpty.bind(to: repositoryTableView.rx.isHidden).disposed(by: disposeBag)
        repositoryListViewModel.outputs.isRepositoriesEmpty.map { (isEmpty) -> Bool in
            !isEmpty
        }.bind(to: noRepositoriesLabel.rx.isHidden).disposed(by: disposeBag)
    }

    func subscribeToRepositorySelection() {
        Observable
            .zip(repositoryTableView.rx.itemSelected, repositoryTableView.rx.modelSelected(InternalRepository.self))
            .bind { [weak self] _, repository in
                self?.repositoryListViewModel.outputs.selectedRepository.onNext(repository)
        }
        .disposed(by: disposeBag)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
