//
//  ProfileSettingsViewController.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import UIKit

class ProfileSettingsViewController: BaseViewController {

    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    var profileSettingsViewModel: ProfileSettingsViewModel!
    init?(coder: NSCoder, profileSettingsViewModel: ProfileSettingsViewModel) {
        self.profileSettingsViewModel = profileSettingsViewModel
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeForAppError(viewModel: profileSettingsViewModel)
        subscribeForDataState(viewModel: profileSettingsViewModel)
        profileSettingsViewModel.outputs.currentUser.bind(to: profileName.rx.text).disposed(by: disposeBag)
        logoutButton.rx.tap.subscribe { [weak self] (_) in
            self?.profileSettingsViewModel.inputs.logoutCurrentUser.onNext(())
        }.disposed(by: disposeBag)
        profileSettingsViewModel.inputs.loadCurrentUser.onNext(())
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
