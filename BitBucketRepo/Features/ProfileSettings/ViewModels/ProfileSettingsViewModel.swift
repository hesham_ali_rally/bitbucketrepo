//
//  ProfileSettingsViewModel.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftKeychainWrapper
final class ProfileSettingsInputs {
    let loadCurrentUser = PublishSubject<Void>()
    let logoutCurrentUser = PublishSubject<Void>()
}

final class ProfileSettingsOutputs {
    let currentUser = BehaviorRelay<String>(value: "")
    let navigateToLogin = PublishSubject<Void>()
}

class ProfileSettingsViewModel: BaseViewModel<ProfileSettingsInputs, ProfileSettingsOutputs> {
    private let userRepo: UserRepo
    init(userRepo: UserRepo) {
        self.userRepo = userRepo
        let profileSettingsInputs = ProfileSettingsInputs()
        let profileSettingsOutputs = ProfileSettingsOutputs()
        super.init(inputs: profileSettingsInputs, outputs: profileSettingsOutputs)
        listenToLoadCurrentUser()
        listenToLogout()
    }

    func listenToLoadCurrentUser() {
        listenWithDisposing(disposable: inputs.loadCurrentUser.subscribe(onNext: { [weak self] (_) in
            guard let self = self else {
                return
            }
            let username = KeychainWrapper.standard.string(forKey: Constants.KeyChainKeys.username)
            let password = KeychainWrapper.standard.string(forKey: Constants.KeyChainKeys.password)
            self.listenWithDisposing(disposable: self.manageViewState(publisher: self.userRepo.fetchUser(username: username ?? "", password: password ?? "")).subscribe { (user, _) in
                self.outputs.currentUser.accept(user?.displayName ?? "")
            })

        }))
    }

    func listenToLogout() {
        listenWithDisposing(disposable: inputs.logoutCurrentUser.subscribe { [weak self] (_) in
            KeychainWrapper.standard.removeAllKeys()
            self?.outputs.navigateToLogin.onNext(())
        })

    }
}
