//
//  RepositoryDetailsViewController.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import UIKit

class RepositoryDetailsViewController: BaseViewController {
    var repositoryDetailsViewModel: RepositoryDetailsViewModel!
    init?(coder: NSCoder, repositoryDetailsViewModel: RepositoryDetailsViewModel) {
        self.repositoryDetailsViewModel = repositoryDetailsViewModel
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    @IBOutlet weak var repositoryTitle: UILabel!
    @IBOutlet weak var creationDate: UILabel!
    @IBOutlet weak var repositoryOwner: UILabel!
    @IBOutlet weak var repositoryVisibility: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        repositoryDetailsViewModel.outputs.createdAt.bind(to: creationDate.rx.text).disposed(by: disposeBag)
        repositoryDetailsViewModel.outputs.repoName.bind(to: repositoryTitle.rx.text).disposed(by: disposeBag)
        repositoryDetailsViewModel.outputs.repoOwner.bind(to: repositoryOwner.rx.text).disposed(by: disposeBag)
        repositoryDetailsViewModel.outputs.repoVisibility.bind(to: repositoryVisibility.rx.text).disposed(by: disposeBag)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
