//
//  ReppositoryDetailsViewModel.swift
//  BitBucketRepo
//
//  Created by Hesham Ali on 9/18/21.
//

import Foundation
import RxSwift
import RxCocoa
final class RepositoryDetailsInputs {
    let repositorySelected = PublishSubject<InternalRepository>()
}

final class RepositoryDetailsOutputs {
    let repoName = BehaviorRelay<String>(value: "")
    let createdAt = BehaviorRelay<String>(value: "")
    let repoOwner = BehaviorRelay<String>(value: "")
    let repoVisibility = BehaviorRelay<String>(value: "")

}

enum Visibility: String {
    case pvt = "Private"
    case plc = "Public"
}

class RepositoryDetailsViewModel: BaseViewModel<RepositoryDetailsInputs, RepositoryDetailsOutputs> {
    init() {
        let repositoryInputs = RepositoryDetailsInputs()
        let repositoryOutputs = RepositoryDetailsOutputs()
        super.init(inputs: repositoryInputs, outputs: repositoryOutputs)
        listenWithDisposing(disposable: inputs.repositorySelected.subscribe(onNext: { [weak self] (repository) in
            self?.outputs.repoName.accept(repository.fullName)
            self?.outputs.createdAt.accept(repository.createdOn.toDateString())
            self?.outputs.repoOwner.accept(repository.owner.displayName)
            self?.outputs.repoVisibility.accept(repository.isPrivate ? Visibility.pvt.rawValue : Visibility.plc.rawValue)
        }))
    }
}
